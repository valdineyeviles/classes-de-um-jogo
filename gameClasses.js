class Hero{
    constructor(nome, idade, classe){
        this.nome = nome
        this.idade = idade
        this.classe = classe
    }

    tipoDeAtaque(){
        let ataque = "espada"
        if (this.classe == "Mago") {
            ataque = "magia";
        } else if (this.classe == "Monge") {
            ataque = "artes marciais";
        } else if (this.classe == "Ninja") {
            ataque = "shuriken"
        } else{
            ataque = ataque;
        }       
        return ataque;
    }

    calcularDanoDaClasse(){
        let dano = 0;
        switch (this.classe) {
            case 'Guerreiro':
                dano = 10;
                break;
            case 'Mago':
                dano = 8;
                break;
            case 'Monge':
                dano = 12;
                break;
            case 'Ninja':
                dano = 15;
                break;
            default:
                console.log("Parece que essa nao e uma classe valida..")
        }
        return dano;
    }

    atacar(){
        console.log(`o ${this.classe} atacou usando ${this.tipoDeAtaque()} e causou ${this.calcularDanoDaClasse()} de dano!`)
    }

}

let Guerreiro = new Hero("Eviles", "24", "Monge")
Guerreiro.atacar()